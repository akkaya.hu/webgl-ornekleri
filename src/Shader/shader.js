class CreateProgram {
  constructor() {

  }
  Create2DProgram(gl){

    const vsSource = `
    attribute vec3 aVertexPosition;
    attribute vec4 aVertexColor;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main(void) {
      gl_Position = uModelViewMatrix * vec4(aVertexPosition,1);
    }
    `

    const fsSource = `
    uniform lowp vec4 vColor;

    void main(void) {
      gl_FragColor = vColor;
    }
    `
    return this.CompileAndReturn(vsSource,fsSource,gl)
  }

  Create3DProgram(gl){

    const vsSource = `
    attribute vec3 aVertexPosition;
    attribute vec4 aVertexColor;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main(void) {
      gl_PointSize=12.0;
      gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition,1);
    }
    `

    const fsSource = `
    uniform lowp vec4 vColor;

    void main(void) {
      gl_FragColor = vColor;
    }
    `
    return this.CompileAndReturn(vsSource,fsSource,gl)
  }

  CreateProgramSphere(gl){
    const vsSource = `
    attribute vec3 coord;
    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    uniform float angleRate;
    uniform float radius;
    uniform float incRate;

    void main(void) {
      vec3 tempC;

      tempC.y= sin(incRate)*radius;
      tempC.z= cos(incRate)*sin(angleRate)*radius;
      tempC.x= cos(incRate)*cos(angleRate)*radius;

      gl_PointSize=2.0;
      gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(
      coord.x+tempC.x,
      coord.y+tempC.y,
      coord.z+tempC.z,1);
    }
    `

    const fsSource = `
    uniform lowp vec4 vColor;

    void main(void) {
      gl_FragColor = vColor;
    }
    `
    return this.CompileAndReturn(vsSource,fsSource,gl)
  }

  CompileAndReturn(vsSource,fsSource,gl){

    const vertShader = gl.createShader(gl.VERTEX_SHADER)
    gl.shaderSource(vertShader,vsSource)
    gl.compileShader(vertShader)

    const fragShader = gl.createShader(gl.FRAGMENT_SHADER)
    gl.shaderSource(fragShader,fsSource)
    gl.compileShader(fragShader)

    const shaderProgram = gl.createProgram()
    gl.attachShader(shaderProgram, vertShader)
    gl.attachShader(shaderProgram, fragShader)
    gl.linkProgram(shaderProgram)

    return shaderProgram
  }

}

export { CreateProgram }
