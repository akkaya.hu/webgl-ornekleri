import { CreateProgram } from "../Shader/shader.js"
import { mat4, mat3, glMatrix} from "../lib/Math/gl-matrix.js"

class Painter {
  constructor() {
    this.createProgram =  new CreateProgram()
    this.canvas=document.createElement("canvas")
    this.canvas.width=1280
    this.canvas.height=640
    document.body.append(this.canvas)
    this.gl=this.canvas.getContext("webgl")
    this.shader2DProgram= this.createProgram.Create2DProgram(this.gl)
    this.shader3DProgram= this.createProgram.Create3DProgram(this.gl)
    this.shaderSphereProgram= this.createProgram.CreateProgramSphere(this.gl)
  }

  SetGlParameter(){
    this.gl.viewport(0,0,this.canvas.width, this.canvas.height)
    this.gl.enable(this.gl.DEPTH_TEST)
    this.gl.clearColor(0.4689,0.5006,1,1)
    this.gl.clear(this.gl.COLOR_BUFFER_BIT)
  }

  Use2DProgram(){
    this.gl.useProgram(this.shader2DProgram)
    this.vertexPos= this.gl.getAttribLocation(this.shader2DProgram,"aVertexPosition")
    this.ModelLoc = this.gl.getUniformLocation(this.shader2DProgram,"uModelViewMatrix")
    this.colorPos= this.gl.getUniformLocation(this.shader2DProgram,"vColor")
    this.gl.enableVertexAttribArray(this.vertexPos)
  }

  Use3DProgram(){
    this.gl.useProgram(this.shader3DProgram)
    this.vertexPos= this.gl.getAttribLocation(this.shader3DProgram,"aVertexPosition")
    this.ModelLoc = this.gl.getUniformLocation(this.shader3DProgram,"uModelViewMatrix")
    this.ProjLoc = this.gl.getUniformLocation(this.shader3DProgram,"uProjectionMatrix")
    this.colorPos= this.gl.getUniformLocation(this.shader3DProgram,"vColor")
    this.gl.enableVertexAttribArray(this.vertexPos)
  }

  UseSphereProgram(){
    this.gl.useProgram(this.shaderSphereProgram)
    this.vertexPos= this.gl.getAttribLocation(this.shaderSphereProgram,"coord")
    this.ModelLoc = this.gl.getUniformLocation(this.shaderSphereProgram,"uModelViewMatrix")
    this.ProjLoc = this.gl.getUniformLocation(this.shaderSphereProgram,"uProjectionMatrix")
    this.colorPos= this.gl.getUniformLocation(this.shaderSphereProgram,"vColor")
    //this.angle= this.gl.getUniformLocation(this.shaderSphereProgram,"angle")
    this.angleRate= this.gl.getUniformLocation(this.shaderSphereProgram,"angleRate")
    this.incRate= this.gl.getUniformLocation(this.shaderSphereProgram,"incRate")
    this.radius= this.gl.getUniformLocation(this.shaderSphereProgram,"radius")
    this.gl.enableVertexAttribArray(this.vertexPos)
  }

  SetPersPective(fovy, aspect, near, far){
    this.ProjectionMatrix = new Float32Array(16)
    mat4.perspective(this.ProjectionMatrix, fovy, aspect, near, far)
  }

  SetLookMatrix(){
    this.lookMatrix = new Float32Array(16)
    // z 100 den 0,0,0 a baksın y up olsun
    mat4.lookAt(this.lookMatrix, [0,0,-200], [0,0,200], [0,1,0])
    console.log(this.lookMatrix);
    mat4.invert(this.lookMatrix,this.lookMatrix.slice())
    console.log(this.lookMatrix);
    //mat4.translate(this.lookMatrix,this.lookMatrix.slice(),[0,0,0])
  }

  SetModelOrthoMatris(left,right,top,bottom,near,far){
    this.ModelMatrix = new Float32Array(16)
    mat4.ortho(this.ModelMatrix,left,right,top,bottom,near,far)
  }

  CreateBuffer(vertexArr){
    var buffer = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(vertexArr), this.gl.STATIC_DRAW)
    return buffer
  }

  DeleteBuffer(buffer){
    this.gl.deleteBuffer(buffer)
  }

  SetUniform3dMatrix(){
    this.gl.uniformMatrix4fv(this.ProjLoc, false, this.ProjectionMatrix)
    this.gl.uniformMatrix4fv(this.ModelLoc, false, this.lookMatrix)
  }

  SetColorUniform(color){
    this.gl.uniform4fv(this.colorPos, color)
  }

  Draw3DLine(vertexBuffer,start,count){
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER,vertexBuffer)
    this.gl.vertexAttribPointer(this.vertexPos, 3, this.gl.FLOAT, false, 0 ,0)
    this.gl.drawArrays(this.gl.LINE_STRIP,start,count)
  }

  Draw3DTriangle(vertexBuffer,start,count){
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER,vertexBuffer)
    this.gl.vertexAttribPointer(this.vertexPos, 3, this.gl.FLOAT, false, 0 ,0)
    this.gl.drawArrays(this.gl.TRIANGLE,start,count)
  }

  DrawSphere(){
    this.SetGlParameter()
    this.UseSphereProgram()
    this.SetPersPective(Math.PI/3,this.canvas.width/this.canvas.height,-200,200)
    this.SetLookMatrix()
    this.SetUniform3dMatrix()

    this.gl.uniform4fv(this.colorPos, [1,0,0,1])
    this.gl.uniform1f(this.radius,75.0)

    var buffer = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(1600*3),this.gl.STATIC_DRAW)
    this.gl.vertexAttribPointer(this.vertexPos, 3, this.gl.FLOAT, false, 0 ,0)

    var incRateZ = Math.PI/40
    var incRate = Math.PI/40
    for(var i =0; i<40; i++){
      this.gl.uniform1f(this.incRate,-Math.PI/2+incRateZ*i)
      for(var j=0; j<40; j++){
        this.gl.uniform1f(this.angleRate,incRate*j)
        this.gl.drawArrays(this.gl.POINT, i*20+j, 1)
      }
    }

  }
}

export { Painter }
